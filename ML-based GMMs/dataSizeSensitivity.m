%Train selected model with given percent of data and test it on the whole data calculation RMSE and repeating
%the process for given number of times. (PGA) 
% * For ZR18 check dataSizeSensitivity_paper.m
% **No Random effect included at the time
clear;
clc;
%% Options and dataSet

%%%%%%%%%%%%%%% Options %%%%%%%%%%%%%%%
%Fraction of data to be used as train data
dataFraction = 10; % 0-100
%Repeating the process
numTest = 20; 

%Default values
options.useLnRV = 0;
options.useMapstd = 0;
options.useMapminmax = 0;

%Regression Method: "RF"|"ANN"|"SVR"|"Paper"|"All" %for using all four
method = "All";
if(method=="All")
    method=["RF","ANN","SVR"];
end

%Model Options
predictorNames = {'Mw', 'Vs30', 'Rhypo'};

RF.numTrees = 160;             
RF.MinLeafSize = 2;            
RF.InBagFraction = 0.75;       
RF.NumPredictorstoSample = 2;  
RF.TrainMethod = 2;            %1: Treebagger | 2: Fitrensemble
RF.Prune = 'off';              %Default: off
RF.useMapstd = 0;

SVR.KernelScale = 1.5;
SVR.Epsilon = 0.15;
SVR.BoxConstraint = 5;
SVR.useLnRV = 1;
    
ANN.hidLaySize = 4;
ANN.trainFunc = 'trainbr';  %'trainlm' | 'trainbr' | 'trainscg'
processFunc = {'removeconstantrows','mapminmax'};   %Pre/Post Process: 'mapminmax'|'mapstd'
ANN.inProcessFunc = processFunc;
ANN.outProcessFunc = processFunc;
ANN.hidTransferFcn = 'logsig';  %'tansig'|'logsig'|'purelin'
ANN.outTransferFcn = 'purelin'; %'tansig'|'logsig'|'purelin'

%Database
load 'Raw_data.mat'

dataN2 = table;
dataN2.Mw = Raw_data.Magnitude;
dataN2.Vs30 = Raw_data.VS30_m_s_;
dataN2.Rhypo = Raw_data.HypocentralDistance_km_;
dataN2.PGA = log(980.665*Raw_data.PGA_g_);
% dataN2.PGA = log(Raw_data.T1_00s);
%%
dataSet = dataN2;
%Remove Nans
dataSet= dataSet(~any(ismissing(dataSet),2),:);
rowIndex = 1:height(dataSet);
dataSet.RowNum = rowIndex';
dataSet.newPGA = dataSet.PGA; %Not needed here but added for compatibility

%RMSE Table
[RFe,ANNe,SVRe,papere] = deal(zeros(numTest+1,1));
RMSEs = table(RFe,ANNe,SVRe,papere);

for j = 1:length(method)
    switch method(j)
        case "RF"
            options = RF;
        case "ANN"
            options = ANN;
        case "SVR"
            options = SVR;
    end
    RMSE = zeros(numTest,1);
    for z = 1:numTest
        
        %Selecting a random fraction of data
        trainDataSet = dataSet(randperm(height(dataSet),floor(height(dataSet)*dataFraction/100)),:);

        if(method(j) == "RF")
            if(options.TrainMethod == 1)
                Model{j} = TreeBagger(options.numTrees,trainDataSet,'PGA','Method','regression','MinLeafSize',options.MinLeafSize,'PredictorNames',predictorNames,...
                    'NumPredictorstoSample',options.NumPredictorstoSample,'InBagFraction',options.InBagFraction,'OOBPrediction','on',...
                    'Prune',options.Prune);
            elseif(options.TrainMethod == 2)
                [~ ,~ ,Model{j}] = trainRF(trainDataSet,options.numTrees,options.MinLeafSize,...
                    options.NumPredictorstoSample,options.InBagFraction);
            end
            dataSet.PredictedPGA = predict(Model{j},dataSet);
        elseif(method(j) == "ANN")
            Model{j} = trainANN(trainDataSet{:,1:3}, trainDataSet.PGA, options.trainFunc ,options.hidLaySize,options.inProcessFunc,options.outProcessFunc,options.hidTransferFcn,options.outTransferFcn);
            dataSet.PredictedPGA = Model{j}(dataSet{:,1:3}')';
        elseif(method(j) == "SVR")
           Model{j} = fitrsvm(trainDataSet,'PGA','KernelFunction','gaussian','KernelScale',options.KernelScale,...
                'Epsilon',options.Epsilon,'BoxConstraint',options.BoxConstraint,'Standardize',true,'PredictorNames',predictorNames);
            dataSet.PredictedPGA = predict(Model{j},dataSet);
        end
        
        RMSE(z) = std(dataSet.PGA - dataSet.PredictedPGA);
        if(z == numTest)
            
        end
    end
    RMSE(z+1) = mean(RMSE(1:z));
    switch method(j)
        case "RF"
            RMSEs.RFe = RMSE;
        case "ANN"
            RMSEs.ANNe = RMSE;
        case "SVR"
            RMSEs.SVRe = RMSE;
    end
end
disp(RMSEs)