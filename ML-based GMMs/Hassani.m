
function [vector, FENA] = Hassani(M, Vs30, R, T)
%%--- Hassani factors
Factor = [-1 0 10.00 7.69 6.25 5.00 4.00 3.13 2.50 2.00 1.59 1.27 1.00 0.79 0.63 0.50 0.40 0.32 0.25 0.20 0.16 0.13 0.10 0.08 0.06 0.05;...
    0.166 0.384 0.065 0.029 0.01 0.025 0.052 0.067 0.061 0.041 0.02 -0.009 -0.043 -0.067 -0.077 -0.068 -0.053 -0.036 -0.016 0.014 0.074 0.158 0.264 0.37 0.439 0.472;...
    0.0007 0.0017 0.0006 0.0006 0.0006 0.0006 0.0006 0.0005 0.0004 0.0004 0.0005 0.0006 0.0009 0.0011 0.0013 0.0014 0.0015 0.0016 0.0018 0.002 0.0021 0.0022 0.0023 0.0024 0.0023 0.0022;...
    0.73 0.63 0.22 0.27 0.27 0.22 0.21 0.27 0.36 0.44 0.48 0.5 0.54 0.57 0.59 0.63 0.71 0.8 0.86 0.89 0.88 0.81 0.71 0.61 0.52 0.44];
num = find(Factor(1,:)==T);
if isempty(num)
    display('Error: no period has been found in Hassani study')
    vector = zeros(1,length(R));
else
    CF1 = Factor(2,num);
    CF2 = Factor(3,num);
    CF3 = Factor(4,num);
    %%--- Calculation:
    for j=1:length(R)
        [median(j), sigma(j), period1(j)] = BSSA_2014_nga(M(j), T, R(j), 0, 0, 999 , Vs30(j));
        logFENA(j)=CF1+CF2*R(j)+CF3*max(0,log10(min(R(j),150)/50));
        FENA(j)=10^(logFENA(j));
        vector(j)=FENA(j)*median(j);
    end
end
end


