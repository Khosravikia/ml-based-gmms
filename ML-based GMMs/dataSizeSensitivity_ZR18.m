% Same as dataSizeSensitivity.m for ZR18 (Calculate coefficients) applying data fraction
clear;
clc;
%%
%%%%%%%%options%%%%%%%%%
removeNaN = 0;
numIteration = 5; %Number of iterations to find coefficients
dataFraction = 10; % 0-100 %
numTest = 2; %Repeating the whole process


Mb = 3.393;
Rb=16.73;
Vc = 638.08;

%Getting table ready !!Warining: order is important!!!
load Raw_data.mat
dataN2 = table;
dataN2.Mw = Raw_data.Magnitude;
dataN2.Vs30 = Raw_data.VS30_m_s_;
dataN2.Rhypo = Raw_data.HypocentralDistance_km_;
dataN2.PGA = Raw_data.PGA_g_;
dataN2.Index = Raw_data.Index;

%Hold RMSEs
papere = zeros(numTest+1,1);
for z = 1:numTest
    dataSet = dataN2;
    
    %Removing NaNs
    if(removeNaN)
        nan = sum(isnan(dataSet{:,:}),2);
        dataSet.nan = nan;
        dataSet(dataSet.nan>0,:) = [];
    end
    
    %Getting Hassani Results into predicted columns
    HassaniOut = Hassani(dataSet.Mw,dataSet.Vs30,dataSet.Rhypo,0); %0 for PGA
    dataSet.PredictedPGA = HassaniOut';
    dataSet.newPredictedPGA = dataSet.PredictedPGA;
    dataSet.Residual = log(dataSet.PGA)-log(dataSet.PredictedPGA);
    
    fullDataSet = dataSet;
    %Choosing a fraction of dataSet for training
    dataSet = dataSet(randperm(height(dataSet),floor(height(dataSet)*dataFraction/100)),:);
    %% Iteration
    for x = 1:numIteration
        %Calculate Index eta and adding values to dataSet
        ModelFM=fitlme(dataSet,'Residual~1+(1|Index)');
        [eta,etaWtihNames] = randomEffects(ModelFM);
        etaWtihNames.eta = eta;
        dataSet.IndexEta = zeros(height(dataSet),1);
        for i = 1:height(dataSet)
            etaIndex = etaWtihNames(strcmp(etaWtihNames.Level , num2str(dataSet(i,:).Index)),:).eta;
            dataSet(i,:).IndexEta = etaIndex;
        end
        
        %Calculate b0 and b1 for FM
        ModelFM1 = fitlme(dataSet(dataSet.Mw<Mb,:),'IndexEta~1');
        b0 = ModelFM1.Coefficients.Estimate;
        dataSet.IndexEta2 = dataSet.IndexEta-b0;
        dataSet.Mw2 = dataSet.Mw-Mb;
        ModelFM2 = fitlme(dataSet(dataSet.Mw>=Mb,:),'IndexEta2~Mw2-1');
        b1 = ModelFM2.Coefficients.Estimate;
        
        %Aplying FM
        for i = 1 : height(dataSet)
            TempRow = dataSet(i,:);
            %FM
            if(TempRow.Mw<Mb)
                FM = b0;
            else
                FM = b0 + b1*(TempRow.Mw-Mb);
            end
            F = exp(FM);
            dataSet.newPredictedPGA(i) = F * TempRow.PredictedPGA;
            dataSet.Residual = log(dataSet.PGA)-log(dataSet.newPredictedPGA);
        end
        
        %Calculating Residual2 = residual - Indexeta - Cadj =epsilon
        Model=fitlme(dataSet,'Residual~1+(1|Index)');
        Cadj = Model.Coefficients.Estimate;
        [eta,etaWtihNames] = randomEffects(ModelFM);
        etaWtihNames.eta = eta;
        dataSet.Residual2 = zeros(height(dataSet),1);
        for i = 1:height(dataSet)
            etaIndex = etaWtihNames(strcmp(etaWtihNames.Level , num2str(dataSet(i,:).Index)),:).eta;
            dataSet(i,:).Residual2 = dataSet(i,:).Residual - etaIndex;
        end
        dataSet(i,:).Residual2 = dataSet(i,:).Residual2 - Cadj;
        
        %Calculate C for FS
        dataSet.Vs302 = log(dataSet.Vs30/Vc);
        ModelFS = fitlm(dataSet(dataSet.Vs30<Vc,:),'Residual2~Vs302-1');
        C = ModelFS.Coefficients.Estimate;
        
        %Calculate alpha for FR
        dataSet.Rhypo2 = log(dataSet.Rhypo/Rb);
        ModelFR = fitlm(dataSet(dataSet.Rhypo<Rb,:),'Residual2~Rhypo2-1');
        alpha = ModelFR.Coefficients.Estimate;
        
        % %aplying FR
        % for i = 1 : height(dataSet)
        %     TempRow = dataSet(i,:);
        %     minRhypo = 4;
        %     midRhypo = Rb;
        %     if(TempRow.Rhypo<minRhypo)
        %         FR = alpha*log(4/Rb);
        %     elseif(4<TempRow.Rhypo && TempRow.Rhypo<Rb)
        %         FR = alpha*log(TempRow.Rhypo/Rb);
        %     else
        %         FR = 0;
        %     end
        %     F = exp(FR);
        %     dataSet.newPredictedPGA(i) = F * TempRow.newPredictedPGA;
        %     dataSet.Residual = log(dataSet.PGA)-log(dataSet.newPredictedPGA);
        % end
        
        
        % %Calculating Residual3 = residual - Indexeta - Cadj =epsilon (resudal is effected by FR)
        % Model=fitlme(dataSet,'Residual~1+(1|Index)');
        % Cadj = Model.Coefficients.Estimate;
        % [eta,etaWtihNames] = randomEffects(ModelFM);
        % etaWtihNames.eta = eta;
        % dataSet.Residual3 = zeros(height(dataSet),1);
        % for i = 1:height(dataSet)
        %     etaIndex = etaWtihNames(strcmp(etaWtihNames.Level , num2str(dataSet(i,:).Index)),:).eta;
        %     dataSet(i,:).Residual3 = dataSet(i,:).Residual - etaIndex;
        % end
        % dataSet(i,:).Residual3 = dataSet(i,:).Residual3 - Cadj;
        
        
        %Applying FR and FS
        for i = 1 : height(dataSet)
            TempRow = dataSet(i,:);
            
            %FR
            minRhypo = 4;
            midRhypo = Rb;
            if(TempRow.Rhypo<minRhypo)
                FR = alpha*log(4/Rb);
            elseif(4<TempRow.Rhypo && TempRow.Rhypo<Rb)
                FR = alpha*log(TempRow.Rhypo/Rb);
            else
                FR = 0;
            end
            
            %FS
            if(TempRow.Vs30<Vc)
                FS = C*log(TempRow.Vs30/Vc);
            else
                FS = 0;
            end
            F = exp(FR+FS);
            dataSet.newPredictedPGA(i) = F * TempRow.PredictedPGA;
            dataSet.Residual = log(dataSet.PGA)-log(dataSet.newPredictedPGA);
        end
        disp(strcat(" b0: ", num2str(b0)," b1: ", num2str(b1)," alpha: ", num2str(alpha)," C: ", num2str(C)));
    end
    
    
    %Apply FM,FR and FS together to calculate Cadj
    for i = 1 : height(dataSet)
        
        TempRow = dataSet(i,:);
        %FM
        if(TempRow.Mw<Mb)
            FM = b0;
        else
            FM = b0 + b1*(TempRow.Mw-Mb);
        end
        %FR
        minRhypo = 4;
        midRhypo = Rb;
        if(TempRow.Rhypo<minRhypo)
            FR = alpha*log(4/Rb);
        elseif(4<TempRow.Rhypo && TempRow.Rhypo<Rb)
            FR = alpha*log(TempRow.Rhypo/Rb);
        else
            FR = 0;
        end
        
        %FS
        if(TempRow.Vs30<Vc)
            FS = C*log(TempRow.Vs30/Vc);
        else
            FS = 0;
        end
        %F
        F = exp(FM+FR+FS);
        
        %     OutputFs.F(i) = F;
        %     OutputFs.FM(i) = FM;
        %     OutputFs.FR(i) = FR;
        %     OutputFs.FS(i) = FS;
        dataSet.newPredictedPGA(i) = F * TempRow.PredictedPGA;
        dataSet.Residual = log(dataSet.PGA)-log(dataSet.newPredictedPGA);
        
    end
    
    %Calculate Cadj, the final one!
    Model=fitlme(dataSet,'Residual~1+(1|Index)');
    Cadj = Model.Coefficients.Estimate;
    %% Calculate final values
    for i = 1 : height(fullDataSet)
        
        TempRow = fullDataSet(i,:);
        %FM
        if(TempRow.Mw<Mb)
            FM = b0;
        else
            FM = b0 + b1*(TempRow.Mw-Mb);
        end
        %FR
        minRhypo = 4;
        midRhypo = Rb;
        if(TempRow.Rhypo<minRhypo)
            FR = alpha*log(4/Rb);
        elseif(4<TempRow.Rhypo && TempRow.Rhypo<Rb)
            FR = alpha*log(TempRow.Rhypo/Rb);
        else
            FR = 0;
        end
        
        %FS
        if(TempRow.Vs30<Vc)
            FS = C*log(TempRow.Vs30/Vc);
        else
            FS = 0;
        end
        %F
        F = exp(Cadj+FM+FR+FS);
        
        %     OutputFs.F(i) = F;
        %     OutputFs.FM(i) = FM;
        %     OutputFs.FR(i) = FR;
        %     OutputFs.FS(i) = FS;
        fullDataSet.PaperOutput(i) = F * TempRow.PredictedPGA(1);
        
    end
    
    RMSET = std(log(980.665*fullDataSet.PGA)-log(980.665*fullDataSet.PaperOutput));
    papere(z) = RMSET;
    disp(strcat("RMSE: ",num2str(RMSET)," Cadj: ",num2str(Cadj), " Run: ", num2str(z),"/",num2str(numTest)));
end
papere(z+1)=mean(papere(1:z));
disp("ZR18 RMSEs:")
disp(papere)