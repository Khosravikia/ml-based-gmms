%Train one of 4 possible models on each of 4Outputs and save the iterations,models and residuals
clear;
clc;

%% Database read and options set

%File options:
toPlot = 1; %Warning: Plot Function (plot4i) includes save inside it (with different name & folder than Ploti)!
toSaveExcel = 1;
testModel = 0; %Save model with T added to name
toSaveModel = 1;


%Default values
options.useLnRV = 0;
options.useMapstd = 0;
options.useMapminmax = 0;

%Override best iteration (Override calculated best iteration with given value)
bestIteOverride = 2; %to prevent override set to 0)

%%%%%%%%%%%%%%% Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%(Normalizations: options.useMapminmax|options.useMapstd|options.useLnRV)%%
%Connect to excel database?
DataBase = 0;
%Include station as random effect?
options.includeStation = 1;     
%Name of Outputs
nameOutput = ["PGA", "PSA(0.10s)", "PSA(0.50s)","PSA(1.00s)"];
%Maximum number of iterations
maxIteration = 2;
%Regression Method: "RF"|"ANN"|"SVR"|"Lin"
method = "Lin";

%RF Setting:
if(method == "RF")
    options.numTrees = 160;             %Default: 140
    options.MinLeafSize = 2;            %Defualt: 2
    options.InBagFraction = 0.75;       %Default: 1
    options.NumPredictorstoSample = 2;  %Default: 2
    options.TrainMethod = 2;            %1: Treebagger | 2: Fitrensemble
    options.Prune = 'off';              %Default: off
    options.useMapstd = 0;
end

%SVR Setting 
if(method == "SVR")
    options.KernelScale = 1.5;
    options.Epsilon = 0.15;
    options.BoxConstraint = 4;
    options.useLnRV = 1;
end

%ANN Setting:
if(method == "ANN")
    options.hidLaySize = 4;
    %Train Function: 'trainlm' | 'trainbr' | 'trainscg'
    options.trainFunc = 'trainbr';
    %Pre/Post Process: 'mapminmax'|'mapstd'
    processFunc = {'removeconstantrows','mapminmax'};
    options.inProcessFunc = processFunc;
    options.outProcessFunc = processFunc;
    %Transfer Functions: 'tansig'|'logsig'|'purelin'
    options.hidTransferFcn = 'logsig';
    options.outTransferFcn = 'purelin';
end

%Linear Setting
if(method == "Lin")
    options.useLnRV = 1;
    maxIteration = 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Read and/or load database
if(DataBase)
    cd ..
    Raw_data = readtable('GroundMotionDatabase-1.xlsx');
    cd Summary1
    Raw_data(Raw_data.HypocentralDistance_km_<4,:) = [];
    Raw_data(Raw_data.HypocentralDistance_km_>500,:)=[];
    Raw_data(Raw_data.Magnitude<2.8,:)=[];
    save 'Raw_data.mat' Raw_data
end
load 'Raw_data.mat'

%Getting plot data from raw data
Raw_plotData = table(Raw_data.Magnitude , Raw_data.VS30_m_s_ , Raw_data.HypocentralDistance_km_ ,'VariableNames',{'Magnitude','VS30_m_s_','HypocentralDistance_km_'});

%Getting data-set ready
dataN2 = table;
dataN2.Mw = Raw_data.Magnitude;
if(options.useLnRV)
    dataN2.Vs30 = log(Raw_data.VS30_m_s_);
    dataN2.Rhypo = log(Raw_data.HypocentralDistance_km_);
else
    dataN2.Vs30 = Raw_data.VS30_m_s_;
    dataN2.Rhypo = Raw_data.HypocentralDistance_km_;    
end
dataN2.PGA = log(980.665*Raw_data.PGA_g_);
dataN2.T0_10 = log(Raw_data.T0_10s);
dataN2.T0_50 = log(Raw_data.T0_50s);
dataN2.T1_00 = log(Raw_data.T1_00s);
dataN2.Index = Raw_data.Index;
dataN2.Station = Raw_data.Station;

%% Preparing data and Iterations
[Model, lme] = deal(cell(length(nameOutput),1));
for z = 1 : length(nameOutput)
    Model{z} = cell(maxIteration,1);
    lme{z} = cell(maxIteration,1);
end
predictorNames = {'Mw', 'Vs30', 'Rhypo'};
for j = 1 : length(nameOutput)
% for j =  3: 3
    %Get dataSet Ready for iterations
    dataSet = dataN2;
    plotData = Raw_plotData;
    dataSet.Output = dataN2{:,j+3};
    plotData.Output = dataSet.Output;
    dataSet= dataSet(~any(ismissing(dataSet),2),:);
    plotData= plotData(~any(ismissing(dataSet),2),:);
    dataSet.newOutput = dataSet.Output;
    if(options.useMapstd)
        [mapped, MPS] = mapstd(dataSet.Mw');
        dataSet.Mw = mapped';
        [mapped, VPS] = mapstd(dataSet.Vs30');
        dataSet.Vs30 = mapped';
        [mapped, RPS] = mapstd(dataSet.Rhypo');
        dataSet.Rhypo = mapped';
    end
    if(options.useMapminmax)
        [mapped, MPS] = mapminmax(dataSet.Mw');
        dataSet.Mw = mapped';
        [mapped, VPS] = mapminmax(dataSet.Vs30');
        dataSet.Vs30 = mapped';
        [mapped, RPS] = mapminmax(dataSet.Rhypo');
        dataSet.Rhypo = mapped';
    end
    
    %Tables/Cells to save results/models:
    if(options.includeStation)
        [Lh, stdIndex , stdStation , stdEps,RMS ,Ri, LhchangeRate, iterat, sqrtstd] = deal(zeros(maxIteration+1,1));
        LhResults = table(Ri, RMS, stdIndex , stdStation , stdEps,sqrtstd, Lh,iterat,  LhchangeRate);
    else
        [Lh, stdIndex , stdStation , stdEps,RMS ,Ri, LhchangeRate, iterat, sqrtstd] = deal(zeros(maxIteration+1,1));
        LhResults = table(Ri, RMS, stdIndex , stdEps,sqrtstd, Lh,iterat,  LhchangeRate);
    end
    
    
    %Create table to store times
    [fixedT, RET, part1T, part2T, part3T,sumParts, totalT, iterat] = deal(zeros(maxIteration+1,1));
    TResults = table(iterat, fixedT, RET, part1T, part2T, part3T,sumParts, totalT);
    
    %Start point for while
    k = 1;
    LHchangeRate{k} = 100;
    bestLHIteration = 1;
    stopSearchLh = 0;
    [stdEtaIndex,eta,stdEtaStation,residual,stdE,RMSE,R,sqr] = deal(cell(maxIteration,1));
    Likelihood = zeros(maxIteration,1);
    
    while(true && k<=maxIteration)
        TResults.iterat(k) = k;
        tic;
        %Normalization
        if(options.useMapminmax)
            [mapped, PS] = mapminmax(dataSet.newOutput');
            dataSet.newOutput = mapped';
        end
        
        if(options.useMapstd)
            [mapped, PS] = mapstd(dataSet.newOutput');
            dataSet.newOutput = mapped';
        end
        
        TResults.part1T(k) = toc;
        tic;
        %Train and prediction
        if(method == "RF")
            if(options.TrainMethod == 1)
                Model{j}{k} = TreeBagger(options.numTrees,dataSet,'newOutput','Method','regression','MinLeafSize',options.MinLeafSize,'PredictorNames',predictorNames,...
                    'NumPredictorstoSample',options.NumPredictorstoSample,'InBagFraction',options.InBagFraction,'OOBPrediction','on',...
                    'Prune',options.Prune);
            elseif(options.TrainMethod == 2)
                [~ ,KFoldErr{j}{k} ,Model{j}{k}] = trainRF(dataSet,options.numTrees,options.MinLeafSize,...
                    options.NumPredictorstoSample,options.InBagFraction,1);
            end
            dataSet.PredictedOutput = predict(Model{j}{k},dataSet);
        elseif(method == "ANN")
            Model{j}{k} = trainANN(dataSet{:,1:3}, dataSet.newOutput, options.trainFunc ,options.hidLaySize,options.inProcessFunc,options.outProcessFunc,options.hidTransferFcn,options.outTransferFcn);
            dataSet.PredictedOutput = Model{j}{k}(dataSet{:,1:3}')';
        elseif(method == "SVR")
            Model{j}{k} = fitrsvm(dataSet,'newOutput','KernelFunction','gaussian','KernelScale',options.KernelScale,...
                'Epsilon',options.Epsilon,'BoxConstraint',options.BoxConstraint,'Standardize',true,'PredictorNames',predictorNames);
            dataSet.PredictedOutput = predict(Model{j}{k},dataSet);
        elseif(method=="Lin")
            if(options.includeStation)
                Model{j}{k}=fitlme(dataSet,'newOutput~Mw+Vs30+Rhypo+(1|Index)+(1|Station)');
            else
                Model{j}{k}=fitlme(dataSet,'newOutput~Mw+Vs30+Rhypo+(1|Index)');
            end
            dataSet.PredictedOutput = predict(Model{j}{k},dataSet,'Conditional',false);
        end
        TResults.fixedT(k) = toc;
        
        tic;
        %Reverse normalization
        if(options.useMapminmax)
            unmapped = mapminmax('reverse',dataSet.newOutput',PS);
            dataSet.newOutput = unmapped';
            unmapped = mapminmax('reverse',dataSet.PredictedOutput',PS);
            dataSet.PredictedOutput = unmapped';
        end
        if(options.useMapstd)
            unmapped = mapstd('reverse',dataSet.newOutput',PS);
            dataSet.newOutput = unmapped';
            unmapped = mapstd('reverse',dataSet.PredictedOutput',PS);
            dataSet.PredictedOutput = unmapped';
        end
        
        %Storing residual (for random effect regression)
        dataSet.residual = dataSet.Output - dataSet.PredictedOutput;
        TResults.part2T(k) = toc;
        
        tic;
        %Train random effect model
        if(method=="Lin")
            lme{j}{k} = Model{j}{k};
        else
            if(options.includeStation)
                lme{j}{k} = fitlme(dataSet, 'residual ~ 1+(1|Index)+(1|Station)');
            else
                lme{j}{k} = fitlme(dataSet, 'residual ~ 1+(1|Index)');
            end
        end
        TResults.RET(k) = toc;
        
        tic;
        %Calculate random effect parameters
        Likelihood(k) = lme{j}{k}.LogLikelihood;
        [eta{k},etaWtihNames] = randomEffects(lme{j}{k});
        etaWtihNames.eta = eta{k};
        stdEtaIndex{k} = std(etaWtihNames(strcmp(etaWtihNames.Group,'Index'),:).eta);
        if(options.includeStation)
            stdEtaStation{k} = std(etaWtihNames(strcmp(etaWtihNames.Group,'Station'),:).eta);
        end
        
        %Likelihood change
        if(k>1)
            LHchangeRate{k} = (Likelihood(k)-Likelihood(k-1))*100/abs(Likelihood(k-1));
            
            %Select the best iteration when change rate is below 5%
            if(abs(LHchangeRate{k})<5 && ~stopSearchLh)
                bestLHIteration = find(Likelihood == max(Likelihood(1:k-1)));
                stopSearchLh = 1;
            end
        end
        
        %Draw residuals without random effect
        if(k==1)
            dataSet.residual = dataSet.Output - dataSet.PredictedOutput;
            plotData.residual = dataSet.residual;
            if(toPlot)
                ploti4(plotData,dataSet,k,strcat(method,(num2str(j)),"_","S",num2str(options.includeStation),"_4O"),nameOutput{j},1);
            end
        end
        
        %Prepare PGA for next iteration
        for i = 1:length(dataSet.newOutput)
            etaIndex = etaWtihNames(strcmp(etaWtihNames.Level , num2str(dataSet(i,:).Index)),:).eta;
            if(options.includeStation)
                etaStation = etaWtihNames(strcmp(etaWtihNames.Level , dataSet(i,:).Station),:).eta;
                dataSet(i,:).newOutput = dataSet(i,:).Output - etaIndex - etaStation;
            else
                dataSet(i,:).newOutput = dataSet(i,:).OutPut - etaIndex;
            end
        end
        
        
        %Calculating residual (y - f - eta) and RMSE/R/Sqr(etas)
        residual{k} = dataSet.newOutput-dataSet.PredictedOutput;
        stdE{k} = std(residual{k});
        RMSE{k} = std(dataSet.Output - dataSet.PredictedOutput);
        [~, Ri, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = Regi(dataSet.Output,dataSet.PredictedOutput,'Temp');
        R{k} = Ri;
        if(options.includeStation)
            sqr{k} = sqrt(stdEtaIndex{k}^2 + stdEtaStation{k}^2 + stdE{k}^2);
        else
            sqr{k} = sqrt(stdEtaIndex{k}^2 + stdE{k}^2);
        end
        
        
        %Display and save result
        if(options.includeStation)
            disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) , ' stdEtaStation: ' , num2str(stdEtaStation{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}) , ' R: ' ,  num2str(R{k}), ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k), ' sqr: ', num2str(sqr{k})]);
            LhResults{k,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} , stdEtaStation{k} ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
        else
            disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) , ' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}) , ' R: ' ,  num2str(R{k}), ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k), ' sqr: ', num2str(sqr{k})]);
            LhResults{k,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
        end
        
        TResults.part3T(k) = toc;
        TResults.sumParts(k) = TResults.part1T(k) + TResults.part2T(k)+ TResults.part3T(k);
        TResults.totalT(k)= TResults.sumParts(k) + TResults.fixedT(k) + TResults.RET(k);
        
        k = k+1;
    end
    
    %% After iteration
    
    %Check for 20% better likelihood
    bestLh = Likelihood(bestLHIteration);
    for i = (bestLHIteration+1):length(Likelihood)
        changeRate = 100*(Likelihood(i)-bestLh)/abs(bestLh);
        if(changeRate>20)
            disp(['Best Iteration changed from', num2str(bestLHIteration), ' to ' , num2str(i)]);
            bestLHIteration = i;
            bestLh = Likelihood(i);
        end
    end
    k = bestLHIteration;
    
    if(bestIteOverride>0 && maxIteration~=1)
        disp(['Best iteration of ',num2str(bestIteOverride),' overrided with: ',num2str(bestIteOverride)]);
        k = bestIteOverride;
        
    end
    
    %Storing residual of iteration with max LH (storing residual at Raw_data in needed at plots)
    dataSet.residual = residual{k};
    plotData.residual = dataSet.residual;
    
    %Display and save best iteration
    disp('Best likelihood on: ');
    if(options.includeStation)
        disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}), ' stdEtaStation: ' , num2str(stdEtaStation{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}),' R: ' ,  num2str(R{k}) , ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k)]);
        LhResults{maxIteration+1,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} , stdEtaStation{k} ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
    else
        disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}),' R: ' ,  num2str(R{k}) , ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k)]);
        LhResults{maxIteration+1,:} = [R{k},RMSE{k} ,  stdEtaIndex{k}  ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
    end
    if(toPlot)
        ploti4(plotData,dataSet,k,strcat(method,(num2str(j)),"_b_",num2str(k),"S",num2str(options.includeStation),"_4O"),nameOutput{j},1);
    end
    
    %Save time results
    lastRow = maxIteration + 1;
    TResults.fixedT(lastRow) = mean(TResults.fixedT(1:maxIteration));
    TResults.RET(lastRow) = mean(TResults.RET(1:maxIteration));
    TResults.sumParts(lastRow) = mean(TResults.sumParts(1:maxIteration));
    TResults.totalT(lastRow) = mean(TResults.totalT(1:maxIteration));
    
    %Export results to excel
    sum = [LhResults(1,:);LhResults(height(LhResults),:)];
    if(method == "ANN")
        writeRange = 'B2';
    elseif(method == "RF")
        writeRange = 'B4';
    elseif(method == "SVR")
        writeRange = 'B6';
    elseif(method == "Lin")
        writeRange = 'B8';
    end
    
    if(options.includeStation)
        sheet = 'Summary';
    else
        sheet = 'Summary_NoSta';
    end
    sheetName = strcat(method,"_",num2str(options.includeStation));
    saveName = strcat(method,"_4Out_",num2str(options.includeStation));
    if(testModel)
        sheetName = strcat(sheetName,"Test");
        saveName = strcat(saveName,"Test");
    end
    range = ['A',num2str(1 +(j-1)*20)];
    if(toSaveExcel)
        disp('Saving Excels...');
        cd RunResults
%         writetable(sum,'Summary.xlsx','sheet',sheet,'range',writeRange,'WriteVariableNames',false);
        writetable(LhResults,'results_4Out.xlsx','sheet',sheetName,'range',range);
        writetable(TResults,'Times_4Out.xlsx','sheet',sheetName,'range',range);
        cd ..
    end
    
    if(method=="RF")
       Model{j} = {Model{j}{1},Model{j}{2}};
       disp(['Removed model 3-',num2str(maxIteration),' from Model{',num2str(j),'}']);
    end
    disp(['Output ',num2str(j), ' Done!']);
end

if(toSaveModel)
    disp('Saving Model...');
    cd Models;
    save(saveName,'eta','LhResults','lme','Model','options','residual','TResults');
    cd ..;
end
% if(method=="ANN")
%     for i = 1 :length(Model)
%         for j = 1 : length(Model{i})
%             Model{i}{j}.userdata.n = cell(4,1);
%             Model{i}{j}.userdata.n{1} = [Model{i}{j}.IW{1}(1,:),Model{i}{j}.b{1}(1)];
%             Model{i}{j}.userdata.n{2} = [Model{i}{j}.IW{1}(2,:),Model{i}{j}.b{1}(2)];
%             Model{i}{j}.userdata.n{3} = [Model{i}{j}.IW{1}(3,:),Model{i}{j}.b{1}(3)];
%             Model{i}{j}.userdata.n{4} = [Model{i}{j}.IW{1}(4,:),Model{i}{j}.b{1}(4)];
%             Model{i}{j}.userdata.o =  [Model{i}{j}.LW{2},Model{i}{j}.b{2}];
%             Model{i}{j}.userdata.Note = "ni = w11i w12i w13i b1i & o = v1 v2 v3 v4 b";
%         end
%     end
% end

disp('Done!');