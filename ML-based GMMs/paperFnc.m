function [PaperOutput,OutputFs,dataSet] = paperFnc(dataSet,index,toLog)
% Calcuations using ZR18

dataSet = dataSet(:,1:3);
%4 Possible Indexes
indexOutput = [0,0.1,0.5,1];

%Colums to store predicted values
dataSet.PredictedOutput = zeros(height(dataSet),1);
dataSet.PaperOutput = zeros(height(dataSet),1);

%Removing NaNs
nan = sum(isnan(dataSet{:,:}),2);
dataSet.nan = nan;
dataSet(dataSet.nan>0,:) = [];

%Paper coefficients
options.PGA.alpha = -0.6347;
options.PGA.Rb = 16.73;
options.PGA.Mb = 3.393;
options.PGA.b0 = 0.0908;
options.PGA.b1 = -0.3217;
options.PGA.C = 0.4779;
options.PGA.Vc = 638.08;
options.PGA.Cadj = -0.22;

options.T0_10.alpha = -0.4351;
options.T0_10.Rb = 20.16;
options.T0_10.Mb = 3.339;
options.T0_10.b0 = 0.0986;
options.T0_10.b1 = -0.3019;
options.T0_10.C = 0.5151;
options.T0_10.Vc = 623.83;
options.T0_10.Cadj = 0.12;

options.T0_50.alpha = -0.6371;
options.T0_50.Rb = 10.15;
options.T0_50.Mb = 4.214;
options.T0_50.b0 = 0.0244;
options.T0_50.b1 = -0.4728;
options.T0_50.C = 0.9996;
options.T0_50.Vc = 514.98;
options.T0_50.Cadj = 0.39;

options.T1_00.alpha = -0.6030;
options.T1_00.Rb = 9.02;
options.T1_00.Mb = 4.692;
options.T1_00.b0 = 0.0078;
options.T1_00.b1 = -0.7923;
options.T1_00.C = 1.0909;
options.T1_00.Vc = 536.04;
options.T1_00.Cadj = 0.51;

%Table to store Factors
OutputFs =table;
[OutputFs.F,OutputFs.FM,OutputFs.FR,OutputFs.FS] = deal(zeros(height(dataSet),1));


%Getting Hassani Results into predicted columns

HassaniOut = Hassani(dataSet.Mw,dataSet.Vs30,dataSet.Rhypo,indexOutput(index));
dataSet.PredictedOutput = HassaniOut';

%Determining Options
switch index
    case 1
        optionsk = options.PGA;
    case 2
        optionsk = options.T0_10;
    case 3
        optionsk = options.T0_50;
    case 4
        optionsk = options.T1_00;
end

%Calculate paper predictd values
for i = 1 : height(dataSet)
    
    TempRow = dataSet(i,:);
    %FM
    if(TempRow.Mw<optionsk.Mb)
        FM = optionsk.b0;
    else
        FM = optionsk.b0 + optionsk.b1*(TempRow.Mw-optionsk.Mb);
    end
    %FR
    minRhypo = 4;
    midRhypo = optionsk.Rb;
    if(TempRow.Rhypo<minRhypo)
        FR = optionsk.alpha*log(4/optionsk.Rb);
    elseif(4<TempRow.Rhypo && TempRow.Rhypo<optionsk.Rb)
        FR = optionsk.alpha*log(TempRow.Rhypo/optionsk.Rb);
    else
        FR = 0;
    end
    
    %FS
    if(TempRow.Vs30<optionsk.Vc)
        FS = optionsk.C*log(TempRow.Vs30/optionsk.Vc);
    else
        FS = 0;
    end
    %F
    F = exp(optionsk.Cadj+FM+FR+FS);
    
    OutputFs.F(i) = F;
    OutputFs.FM(i) = FM;
    OutputFs.FR(i) = FR;
    OutputFs.FS(i) = FS;
    dataSet.PaperOutput(i) = F * TempRow.PredictedOutput(1);

    
    
end
if(toLog)
    if(index==1)
        dataSet.PredictedOutput = log(980.665*dataSet.PredictedOutput);
        dataSet.PaperOutput = log(980.665*dataSet.PaperOutput);
    else
        dataSet.PredictedOutput = log(dataSet.PredictedOutput);
        dataSet.PaperOutput = log(dataSet.PaperOutput);
    end
end
PaperOutput = dataSet.PaperOutput;