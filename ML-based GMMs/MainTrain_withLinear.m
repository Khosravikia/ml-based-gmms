% Same as MainTrain with added linear with factor method

clear;
clc;

%% Database read and options set

%File options:
toPlot = 1; %Warning: Plot includes save inside its function!
toSaveExcel = 1;
testModel = 0;
toSaveModel = 1;


%Default values
options.useLnRV = 0;
options.useMapstd = 0;
options.useMapminmax = 0;

%%%%%%%%%%%%%%% Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%(Normalizations: options.useMapminmax|options.useMapstd|options.useLnRV)%%
%Connect to excel database?
DataBase = 0;
%Include station as random effect?
options.includeStation = 1;
%Maximum number of iterations
maxIteration = 1;
%Regression Method: "RF"|"ANN"|"SVR"|"Lin"|"FLin"
method = "FLin";

%RF Setting:
if(method == "RF")
    options.numTrees = 140;             %Default: 140
    options.MinLeafSize = 2;            %Defualt: 2
    options.NumPredictorstoSample = 2;  %Default: 2
    options.InBagFraction = 1;        %Default: 1
    options.Prune = 'off';               %Default: off
    options.useMapstd = 0;
end

%SVR Setting 
if(method == "SVR")
    options.KernelScale = 0.5;
    options.Epsilon = 0.15;
    options.BoxConstraint = 2;
    options.useLnRV = 1;
end

%ANN Setting:
if(method == "ANN")
    options.hidLaySize = 12;
    %Train Function: 'trainlm' | 'trainbr' | 'trainscg'
    options.trainFunc = 'trainbr';
    %Pre/Post Process: 'mapminmax'|'mapstd'
    processFunc = {'removeconstantrows','mapstd'};
    options.inProcessFunc = processFunc;
    options.outProcessFunc = processFunc;
    %Transfer Functions: 'tansig'|'logsig'
    options.hidTransferFcn = 'tansig';
    options.outTransferFcn = 'purelin';
end

%Linear Setting
if(method == "Lin")
    options.useLnRV = 1;
    maxIteration = 1;
end

%Linear with factor setting
if(method=="FLin")
    options.PGA.alpha = -0.6347;
    options.PGA.Rb = 16.73;
    options.PGA.Mb = 3.393;
    options.PGA.b0 = 0.0908;
    options.PGA.b1 = -0.3217;
    options.PGA.C = 0.4779;
    options.PGA.Vc = 638.08;
    options.PGA.Cadj = -0.22;
    
    options.useLnRV = 0;
    maxIteration = 1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Read and/or load database
if(DataBase)
    cd ..
    Raw_data = readtable('GroundMotionDatabase-1.xlsx');
    cd Summary1
    Raw_data(Raw_data.HypocentralDistance_km_<4,:) = [];
    Raw_data(Raw_data.HypocentralDistance_km_>500,:)=[];
    Raw_data(Raw_data.Magnitude<2.8,:)=[];
    save 'Raw_data.mat' Raw_data
end
load 'Raw_data.mat'

%Getting data-set ready
dataN2 = table;
dataN2.Mw = Raw_data.Magnitude;
if(options.useLnRV)
    dataN2.Vs30 = log(Raw_data.VS30_m_s_);
    dataN2.Rhypo = log(Raw_data.HypocentralDistance_km_);
else
    dataN2.Vs30 = Raw_data.VS30_m_s_;
    dataN2.Rhypo = Raw_data.HypocentralDistance_km_;    
end
dataN2.PGA = log(980.665*Raw_data.PGA_g_);
dataN2.Index = Raw_data.Index;
dataN2.Station = Raw_data.Station;

%% Preparing data and Iterations

%Get dataSet Ready for iterations
dataSet = dataN2;
dataSet.newPGA = dataSet.PGA;

predictorNames = {'Mw', 'Vs30', 'Rhypo'};
if(options.useMapstd)
    [mapped, MPS] = mapstd(dataSet.Mw');
    dataSet.Mw = mapped';
    [mapped, VPS] = mapstd(dataSet.Vs30');
    dataSet.Vs30 = mapped';
    [mapped, RPS] = mapstd(dataSet.Rhypo');
    dataSet.Rhypo = mapped';
end
if(options.useMapminmax)
    [mapped, MPS] = mapminmax(dataSet.Mw');
    dataSet.Mw = mapped';
    [mapped, VPS] = mapminmax(dataSet.Vs30');
    dataSet.Vs30 = mapped';
    [mapped, RPS] = mapminmax(dataSet.Rhypo');
    dataSet.Rhypo = mapped';
end

%Tables/Cells to save results/models:
if(options.includeStation)
    [Lh, stdIndex , stdStation , stdEps,RMS ,Ri, LhchangeRate, iterat, sqrtstd] = deal(zeros(maxIteration+1,1));
    LhResults = table(Ri, RMS, stdIndex , stdStation , stdEps,sqrtstd, Lh,iterat,  LhchangeRate);
else
    [Lh, stdIndex , stdStation , stdEps,RMS ,Ri, LhchangeRate, iterat, sqrtstd] = deal(zeros(maxIteration+1,1));
    LhResults = table(Ri, RMS, stdIndex , stdEps,sqrtstd, Lh,iterat,  LhchangeRate);
end
[Model, lme] = deal(cell(maxIteration,1));

%Create table to store times
[fixedT, RET, part1T, part2T, part3T,sumParts, totalT, iterat] = deal(zeros(maxIteration+1,1));
TResults = table(iterat, fixedT, RET, part1T, part2T, part3T,sumParts, totalT);

%Start point for while
k = 1;
LHchangeRate{k} = 100;
bestLHIteration = 1;
stopSearchLh = 0;
[stdEtaIndex,eta,stdEtaStation,residual,stdE,RMSE,R,sqr] = deal(cell(maxIteration,1));
Likelihood = zeros(maxIteration,1);

while(true && k<=maxIteration)
    TResults.iterat(k) = k;
    tic;
    %Normalization
    if(options.useMapminmax)
       [mapped, PS] = mapminmax(dataSet.newPGA'); 
       dataSet.newPGA = mapped';
    end
        
    if(options.useMapstd)
       [mapped, PS] = mapstd(dataSet.newPGA'); 
       dataSet.newPGA = mapped';
    end
    
    TResults.part1T(k) = toc;
    tic;
    %Train and prediction
    if(method == "RF")
        Model{k} = TreeBagger(options.numTrees,dataSet,'newPGA','Method','regression','MinLeafSize',options.MinLeafSize,'PredictorNames',predictorNames,...
            'NumPredictorstoSample',options.NumPredictorstoSample,'InBagFraction',options.InBagFraction,'OOBPrediction','on',...
            'Prune',options.Prune);
        dataSet.PredictedPGA = predict(Model{k},dataSet);
    elseif(method == "ANN")
        Model{k} = trainANN(dataSet{:,1:3}, dataSet.newPGA, options.trainFunc ,options.hidLaySize,options.inProcessFunc,options.outProcessFunc,options.hidTransferFcn,options.outTransferFcn);
        dataSet.PredictedPGA = Model{k}(dataSet{:,1:3}')';
    elseif(method == "SVR")
        Model{k} = fitrsvm(dataSet,'newPGA','KernelFunction','gaussian','KernelScale',options.KernelScale,...
            'Epsilon',options.Epsilon,'BoxConstraint',options.BoxConstraint,'Standardize',true,'PredictorNames',predictorNames);
        dataSet.PredictedPGA = predict(Model{k},dataSet);
    elseif(method=="Lin")
        if(options.includeStation)
            Model{k}=fitlme(dataSet,'newPGA~Mw+Vs30+Rhypo+(1|Index)+(1|Station)');
        else
            Model{k}=fitlme(dataSet,'newPGA~Mw+Vs30+Rhypo+(1|Index)');
        end
        dataSet.PredictedPGA = predict(Model{k},dataSet,'Conditional',false);
    elseif(method=="FLin")
        Model{k} = fitlm(dataSet,'newPGA~Mw+Vs30+Rhypo');
        dataSet.PredictedPGA = predict(Model{k},dataSet);
        
        %Calculate and assign adjusment factors
        [dataSet.F,dataSet.FM,dataSet.FR,dataSet.FS] = deal(zeros(height(dataSet),1));
        for i = 1 : height(dataSet)
            
            if(rem(i,100)==1)
                tic;
            end
            
            TempRow = dataSet(i,:);
            %FM
            if(TempRow.Mw<options.PGA.Mb)
                FM = options.PGA.b0;
            else
                FM = options.PGA.b0 + options.PGA.b1*(TempRow.Mw-options.PGA.Mb);
            end
            %FR
            minRhypo = 4;
            midRhypo = options.PGA.Rb;
            if(TempRow.Rhypo<minRhypo)
                FR = options.PGA.alpha*log(4/options.PGA.Rb);
            elseif(4<TempRow.Rhypo && TempRow.Rhypo<options.PGA.Rb)
                FR = options.PGA.alpha*log(TempRow.Rhypo/options.PGA.Rb);
            else
                FR = 0;
            end
            
            %FS
            if(TempRow.Vs30<options.PGA.Vc)
                FS = options.PGA.C*log(TempRow.Vs30/options.PGA.Vc);
            else
                FS = 0;
            end
            %F
            F = FM+FR+FS;
            dataSet.F(i) = F;
%             dataSet.FM(i) = FM;
%             dataSet.FR(i) = FR;
%             dataSet.FS(i) = FS;
            dataSet.PredictedPGA(i) = F * TempRow.PredictedPGA;

            if(rem(i,100)==0)
                time = toc;
                disp(['i = ',num2str(i),' Time: ' ,num2str(time)]);
            end
            if(i==height(dataSet))
               disp('last'); 
            end
        end
    end
    TResults.fixedT(k) = toc;
    
    tic;
    %Reverse normalization
    if(options.useMapminmax)
       unmapped = mapminmax('reverse',dataSet.newPGA',PS); 
       dataSet.newPGA = unmapped';
       unmapped = mapminmax('reverse',dataSet.PredictedPGA',PS);
       dataSet.PredictedPGA = unmapped';
    end    
    if(options.useMapstd)
       unmapped = mapstd('reverse',dataSet.newPGA',PS); 
       dataSet.newPGA = unmapped';
       unmapped = mapstd('reverse',dataSet.PredictedPGA',PS);
       dataSet.PredictedPGA = unmapped';
    end  
    
    %Storing residual (for random effect regression)
    dataSet.residual = dataSet.PGA - dataSet.PredictedPGA;
    TResults.part2T(k) = toc;
    
    %%%%%%%%%%%%%%%%%%%%%%Separate FLin from others
    if(method=="FLin")
        k = k+1;
    else
    tic;
    %Train random effect model
    if(method=="Lin")
        lme{k} = Model{k};
    else
        if(options.includeStation)
            lme{k} = fitlme(dataSet, 'residual ~ 1+(1|Index)+(1|Station)');
        else
            lme{k} = fitlme(dataSet, 'residual ~ 1+(1|Index)');
        end
    end
    TResults.RET(k) = toc;
    
    tic;
    %Calculate random effect parameters
    Likelihood(k) = lme{k}.LogLikelihood;
    [eta{k},etaWtihNames] = randomEffects(lme{k});
    etaWtihNames.eta = eta{k};
    stdEtaIndex{k} = std(etaWtihNames(strcmp(etaWtihNames.Group,'Index'),:).eta);
    if(options.includeStation)
        stdEtaStation{k} = std(etaWtihNames(strcmp(etaWtihNames.Group,'Station'),:).eta);
    end
    
    %Likelihood change
    if(k>1)
        LHchangeRate{k} = (Likelihood(k)-Likelihood(k-1))*100/abs(Likelihood(k-1));
        
        %Select the best iteration when change rate is below 5%
        if(abs(LHchangeRate{k})<5 && ~stopSearchLh)
            bestLHIteration = find(Likelihood == max(Likelihood(1:k-1)));
            stopSearchLh = 1;
        end
    end
    
    %Draw residuals without random effect
    if(k==1)
        dataSet.residual = dataSet.PGA - dataSet.PredictedPGA;
        Raw_data.residual = dataSet.residual;
        if(toPlot)
            ploti(Raw_data,dataSet,k,strcat(method,"_","S",num2str(options.includeStation)),0);
        end
    end
    
    %Prepare PGA for next iteration
    for i = 1:length(dataSet.newPGA)
        etaIndex = etaWtihNames(strcmp(etaWtihNames.Level , num2str(dataSet(i,:).Index)),:).eta;        
        if(options.includeStation)
            etaStation = etaWtihNames(strcmp(etaWtihNames.Level , dataSet(i,:).Station),:).eta;
            dataSet(i,:).newPGA = dataSet(i,:).PGA - etaIndex - etaStation;
        else
            dataSet(i,:).newPGA = dataSet(i,:).PGA - etaIndex;
        end 
    end
    
    
    %Calculating residual (y - f - eta) and RMSE/R/Sqr(etas)
    residual{k} = dataSet.newPGA-dataSet.PredictedPGA;
    stdE{k} = std(residual{k});
    RMSE{k} = std(dataSet.PGA - dataSet.PredictedPGA);
    [~, Ri, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = Regi(dataSet.PGA,dataSet.PredictedPGA,'Temp');
    R{k} = Ri;
    if(options.includeStation)
        sqr{k} = sqrt(stdEtaIndex{k}^2 + stdEtaStation{k}^2 + stdE{k}^2);
    else
        sqr{k} = sqrt(stdEtaIndex{k}^2 + stdE{k}^2);
    end
    
    
    %Display and save result
    if(options.includeStation)
        disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) , ' stdEtaStation: ' , num2str(stdEtaStation{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}) , ' R: ' ,  num2str(R{k}), ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k), ' sqr: ', num2str(sqr{k})]);
        LhResults{k,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} , stdEtaStation{k} ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
    else
        disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) , ' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}) , ' R: ' ,  num2str(R{k}), ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k), ' sqr: ', num2str(sqr{k})]);
        LhResults{k,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
    end
    
    TResults.part3T(k) = toc;
    TResults.sumParts(k) = TResults.part1T(k) + TResults.part2T(k)+ TResults.part3T(k);
    TResults.totalT(k)= TResults.sumParts(k) + TResults.fixedT(k) + TResults.RET(k);
    
    k = k+1;
    end
end

%% After iteration
%%%%%%%%%%%%%%%%%%%%%%Separate FLin from others
if(method=="FLin")
    RMSE{k} = std(dataSet.PGA - dataSet.PredictedPGA);
else
%Check for 20% better likelihood
bestLh = Likelihood(bestLHIteration);
for i = (bestLHIteration+1):length(Likelihood)
    changeRate = 100*(Likelihood(i)-bestLh)/abs(bestLh);
    if(changeRate>20)
        disp(['Best Iteration changed from', num2str(bestLHIteration), ' to ' , num2str(i)]);
        bestLHIteration = i;
        bestLh = Likelihood(i);
    end
end
k = bestLHIteration;

%Storing residual of iteration with max LH (storing residual at Raw_data in needed at plots)
dataSet.residual = residual{k};
Raw_data.residual = dataSet.residual;

%Display and save best iteration
disp('Best likelihood on: ');
if(options.includeStation)
    disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}), ' stdEtaStation: ' , num2str(stdEtaStation{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}),' R: ' ,  num2str(R{k}) , ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k)]);
    LhResults{maxIteration+1,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} , stdEtaStation{k} ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
else
    disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}),' R: ' ,  num2str(R{k}) , ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k)]);
    LhResults{maxIteration+1,:} = [R{k},RMSE{k} ,  stdEtaIndex{k}  ,stdE{k} ,sqr{k}, Likelihood(k),k, LHchangeRate{k}];
end
if(toPlot)
    ploti(Raw_data,dataSet,k,strcat(method,"_b_",num2str(k),"S",num2str(options.includeStation)),0);
end

%Save time results
lastRow = maxIteration + 1;
TResults.fixedT(lastRow) = mean(TResults.fixedT(1:maxIteration));
TResults.RET(lastRow) = mean(TResults.RET(1:maxIteration));
TResults.sumParts(lastRow) = mean(TResults.sumParts(1:maxIteration));
TResults.totalT(lastRow) = mean(TResults.totalT(1:maxIteration));

%Export results to excel
sum = [LhResults(1,:);LhResults(height(LhResults),:)];
if(method == "ANN")
    writeRange = 'B2';
elseif(method == "RF")
    writeRange = 'B4';
elseif(method == "SVR")
    writeRange = 'B6';
elseif(method == "Lin")
    writeRange = 'B8';
end

if(options.includeStation)
    sheet = 'Summary';
else
    sheet = 'Summary_NoSta';
end
name = strcat(method,"_",num2str(options.includeStation));
if(testModel)
    name = strcat(name,"Test");
end

if(toSaveExcel)
    disp('Saving Excels...');
    writetable(sum,'Summary.xlsx','sheet',sheet,'range',writeRange,'WriteVariableNames',false);
    writetable(LhResults,'results.xlsx','sheet',name,'range','A1');
    writetable(TResults,'Times.xlsx','sheet',name,'range','A1');
end

if(toSaveModel)
    disp('Saving Model...');
    cd Models;
    save(name,'eta','LhResults','lme','Model','options','residual','TResults');
    cd ..;
end
disp('Done!');
end