%% Regression Function:
function [R_fac, R, MAE, MAPE, MSE, RMSE, k1, k2, Ro2_1, Ro2_2, m_ind, n_ind, ro, n] = Regi(target, estimate, name)
if strfind(name, 'PGV') > 0
    x_1 = 2.4;
    x_2 = -6;
    range = -10:4:10;
    Range = [-10 10];
else
    x_1 = 2.4;
    x_2 = -6;
    range = -10:4:10;
    Range = [-10 10];
end
n=1;
for i=1:length(target)
    if isnan(target(i))==0
        y(n) = target(i);
        z(n) = estimate(i);
        n=n+1;
    end
end

% MAE, MAPE, MSE calculation:
MAE = 0;
MAPE = 0;
MSE = 0;
for ii = 1:length(y)
    MAE = MAE+abs(y(ii)-z(ii))/n;
    MAPE = MAPE + abs((y(ii)-z(ii))/y(ii))/n;
    MSE = MSE + ((y(ii)-z(ii))^2)/n;
end
RMSE = sqrt(MSE);
NRMSE = RMSE/(max(y)-min(y));

% R calculation:
numerator = 0;
denominetor_1 = 0;
denominetor_2 = 0;
for ii = 1:length(y)
    numerator = numerator + (y(ii)-mean(y))*(z(ii)-mean(z));
    denominetor_1 = denominetor_1 + (y(ii)-mean(y))^2;
    denominetor_2 = denominetor_2 + (z(ii)-mean(z))^2;
end
denominetor = sqrt( denominetor_1 * denominetor_2);
R = numerator/denominetor;

ro = NRMSE*(1/(1+R));

% k1 and k2 calculation:
k1_nom = 0; 
k1_den = 0;
k2_nom = 0;
k2_den = 0;
for ii = 1:length(y)
    k1_nom = k1_nom +(y(ii)*z(ii));
    k1_den = k1_den + z(ii)^2;
    k2_nom = k2_nom +(y(ii)*z(ii));
    k2_den = k2_den + y(ii)^2;
end
k1 = k1_nom/k1_den;
k2 = k2_nom/k2_den;

% Ro2_1 calculation:
h0=k1*z;
temp_1 = 0;
temp_2 =0;
for ii = 1:length(y)
    temp_1 = temp_1 +(z(ii)-h0(ii))^2;
    temp_2 = temp_2 +(z(ii)-mean(z))^2;
end
Ro2_1 = 1-temp_1/temp_2;

% Ro2_2 calculation:
t0=k2*y;
temp_1 = 0;
temp_2 =0;
for ii = 1:length(y)
    temp_1 = temp_1 +(y(ii)-t0(ii))^2;
    temp_2 = temp_2 +(y(ii)-mean(y))^2;
end
Ro2_2 = 1-temp_1/temp_2;

% R_factor calculation:
mean1 = mean(y);
SST = 0;
for ii = 1:length(y)
    SST = SST+(y(ii)-mean1)^2;
end

SSE = 0;
for ii = 1:length(y)
    SSE = SSE+(y(ii)-z(ii))^2;
end

R2 = 1-(SSE/SST);
R_fac = sqrt(R2);

% m and n index:
m_ind = (R^2-Ro2_1)/R^2;
n_ind = (R^2-Ro2_2)/R^2;

n = n-1;
% f1 = figure;
% hA1 = subplot(1,1,1);
% scatter(y,z, 40, 'MarkerEdgeColor',[0.5 .5 .5])
% hold on
% plot(Range, Range, '--', 'color', 'black',  'LineWidth',1.5)
% text(x_1,x_2,['{\it R} = ',num2str(round(R*100)/100)], 'FontName','Times New Roman','FontWeight','normal','FontSize',22)
% text(x_1,x_2-1.7,['{\it MAE} = ',num2str(round(MAE*100)/100)], 'FontName','Times New Roman','FontWeight','normal','FontSize',22)
% 
% set(gca,'XGrid','off','YGrid','off','box','on','FontName','Times New Roman','FontWeight','normal','FontSize',18)
% set(hA1, 'Ytick', range, 'YLim', Range);
% set(hA1, 'Xtick', range, 'XLim', Range);
% cd 'RunResults\Figs';
% saveas(f1, [name, '.fig'])
% cd ..;
% cd ..;
% close(f1)

end


