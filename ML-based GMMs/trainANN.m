function [net, tr, PGAby100] = trainANN(input, PGA, trainFucn,hidLaySize,inProcFnc,outProcFnc,hidTransferFcn,outTransferFcn )

% Solve an Input-Output Fitting problem with a Neural Network
%
% This script assumes these variables are defined:
%
%   input - input data.
%   PGA - target data.

x = input';
t = PGA';

% Choose a Training Function
% For a list of all training functions type: help nntrain
% 'trainlm' is usually fastest.
% 'trainbr' takes longer but may be better for challenging problems.
% 'trainscg' uses less memory. Suitable in low memory situations.
trainFcn = trainFucn;  % Levenberg-Marquardt backpropagation.

% Create a Fitting Network
hiddenLayerSize = hidLaySize;
net = fitnet(hiddenLayerSize,trainFcn);

% Choose Input and Output Pre/Post-Processing Functions
% For a list of all processing functions type: help nnprocess
net.input.processFcns = inProcFnc;
net.output.processFcns = outProcFnc;

%Choose transfer function of hidden layer and output later:
net.layers{1}.transferFcn = hidTransferFcn;
net.layers{2}.transferFcn = outTransferFcn;

% Setup Division of Data for Training, Validation, Testing
% For a list of all data division functions type: help nndivision
% net.divideFcn = 'dividerand';  % rand: Divide data randomly
% net.divideMode = 'sample';  % Divide up every sample
% net.divideParam.trainRatio = 70/100;
% net.divideParam.valRatio = 15/100;
% net.divideParam.testRatio = 10/100;

if( strcmp(trainFucn,'trainbr'))
    net.divideParam.trainRatio = 80/100;
    net.divideParam.testRatio = 20/100;
end

PGAby100 = 0;
% Same thing with serial divide
% net.divideFcn = 'divideind';  % rand: Divide data randomly
% net.divideMode = 'sample';  % Divide up every sample
% PGAby100 = floor(length(PGA)/100);
% net.divideParam.trainInd = 1:PGAby100*70;
% net.divideParam.valInd = PGAby100*70:PGAby100*85;
% net.divideParam.testInd = PGAby100*85:length(PGA);


%Set MU (br default: 0.0050 , 0.1000 , 10 10e+10)
% net.trainParam.Mu = 1;
% net.trainParam.Mu_dec = 0.8;
%net.trainParam.Mu_inc = 2;
% net.trainParam.mu_max = 1e+15;

%Set max epochs (default: 1000)
%net.trainParam.epochs = 2000;

%Set max fail (default 6)
% net.trainParam.max_fail = 16;

%Set Regularization (default 0)
%net.performParam.regularization = 0.5;

%Set normalization of error
%net.performParam.normalization = 'standard';

%Set performance function: 'mse' | 'msereg' | 'mae'
%net.performFcn = 'mae';

% Train the Network | 'useParallel','yes'
[net,tr] = train(net,x,t);

% Test the Network
y = net(x);
e = gsubtract(t,y);
performance = perform(net,t,y);

% View the Network
%view(net)

% Plots
% Uncomment these lines to enable various plots.
%figure, plotperform(tr)
%figure, plottrainstate(tr)
%figure, ploterrhist(e)
%figure, plotregression(t,y)
%figure, plotfit(net,x,t)

