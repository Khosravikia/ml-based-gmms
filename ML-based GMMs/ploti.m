
function [] = ploti(Raw_data,dataSet,k,name,closeFig)
f1= figure('Units', 'Normalized', 'OuterPosition', [0, 0.05, 1, 0.59]);
subplot(1,3,1);

% Detecting empty name
if(strcmp(name,''))
    toSave = 0;
else
    toSave = 1;
end

points = logspace(min(log10(Raw_data.HypocentralDistance_km_)), max(log10(Raw_data.HypocentralDistance_km_)), 8);
for ff = 1: length(points)-1
    Temp = Raw_data(Raw_data.HypocentralDistance_km_ > points(ff),:);
    Temp(Temp.HypocentralDistance_km_ > points(ff+1),:) = [];
    residual_ave(ff) = mean(Temp.residual);
    sigma(ff) = std(Temp.residual);
    x_ave(ff) = (points(ff) + points(ff+1))/2;
    clearvars Temp
end

scatter(Raw_data.HypocentralDistance_km_,dataSet.residual,40, 'MarkerEdgeColor',[0.5 .5 .5]);
axis([1 1000 -3 3]);
set(gca,'xscale','log');
xlabel('{\it R}_{hypo} (km)','FontName','Times New Roman','FontName','Times New Roman','Position', [31.6229 -3.33 -1.0000]);
ylabel([char(949) '(PGA)'], 'FontName' , 'Times New Roman');
grid on;
box on;
hold on;
errorbar(x_ave, residual_ave, sigma,'-s','LineWidth', 1.5, 'color', 'black', 'MarkerSize',10,'MarkerEdgeColor','red','MarkerFaceColor','red')


subplot(1,3,2);

points = logspace(min(log10(Raw_data.VS30_m_s_)), max(log10(Raw_data.VS30_m_s_)), 8);
for ff = 1: length(points)-1
    Temp = Raw_data(Raw_data.VS30_m_s_ > points(ff),:);
    Temp(Temp.VS30_m_s_ > points(ff+1),:) = [];
    residual_ave(ff) = mean(Temp.residual);
    sigma(ff) = std(Temp.residual);
    x_ave(ff) = (points(ff) + points(ff+1))/2;
    clearvars Temp
end

scatter(Raw_data.VS30_m_s_,dataSet.residual,40, 'MarkerEdgeColor',[0.5 .5 .5]);
axis([100 2000 -3 3]);
set(gca,'xscale','log');
xlabel('{\it V}_{s30} (m/s)','FontName','Times New Roman','FontName','Times New Roman','Position', [447.2142 -3.335 -1.0000]);
grid on;
box on;
hold on;
errorbar(x_ave, residual_ave, sigma,'-s','LineWidth', 1.5, 'color', 'black', 'MarkerSize',10,'MarkerEdgeColor','red','MarkerFaceColor','red')


subplot(1,3,3);

points = logspace(min(log10(Raw_data.Magnitude)), max(log10(Raw_data.Magnitude)), 6);
for ff = 1: length(points)-1
    Temp = Raw_data(Raw_data.Magnitude > points(ff),:);
    Temp(Temp.Magnitude > points(ff+1),:) = [];
    residual_ave(ff) = mean(Temp.residual);
    sigma(ff) = std(Temp.residual);
    x_ave(ff) = (points(ff) + points(ff+1))/2;
    clearvars Temp
end

scatter(Raw_data.Magnitude,dataSet.residual,40, 'MarkerEdgeColor',[0.5 .5 .5]);
axis([2.5 6 -3 3]);
xlabel('{\it M}_{\it w}','FontName','Times New Roman','FontName','Times New Roman','Position',[4.2500 -3.3 -1.0000]);
grid on;
box on;

hold on;
errorbar(x_ave(1:5), residual_ave(1:5), sigma(1:5),'-s','LineWidth', 1.5, 'color', 'black', 'MarkerSize',10,'MarkerEdgeColor','red','MarkerFaceColor','red');

if(toSave)
    cd 'RunResults\Figs';
    saveas(f1,name);
    saveas(f1,strcat(name,".jpg"));
    cd ..;
    cd ..;
end

if(closeFig)
    close(f1);
end