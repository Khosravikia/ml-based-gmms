%% Read Model
cd Models\
load ANN_4Out_1.mat
cd ..

%% Extract ANN weight considering it is loaded with the name: Model

wrtieToExcel = 1;
Output = 1;
%% Extract weight and save as userdata
for i = 1 :length(Model)
    for j = 1 : length(Model{i})
        Model{i}{j}.userdata.n = cell(4,1);
        Model{i}{j}.userdata.n{1} = [Model{i}{j}.IW{1}(1,:),Model{i}{j}.b{1}(1)];
        Model{i}{j}.userdata.n{2} = [Model{i}{j}.IW{1}(2,:),Model{i}{j}.b{1}(2)];
        Model{i}{j}.userdata.n{3} = [Model{i}{j}.IW{1}(3,:),Model{i}{j}.b{1}(3)];
        Model{i}{j}.userdata.n{4} = [Model{i}{j}.IW{1}(4,:),Model{i}{j}.b{1}(4)];
        Model{i}{j}.userdata.o =  [Model{i}{j}.LW{2},Model{i}{j}.b{2}];
        Model{i}{j}.userdata.Note = "ni = w11i w12i w13i b1i & o = v1 v2 v3 v4 b";
    end
end
%% Create the tables and write them to excel
neuron = cell(4,1);
Output = ["PGA";"PSA (0.10s)";"PSA (0.50s)";"PSA (1.00s)"];
[w11,w21,w31,b1,v1,v2,v3,v4,b] = deal(zeros(4,1));
outNeuron = table(Output,v1,v2,v3,v4,b);
for i = 1 : 4
    
    neuron{i} = table(Output,w11,w21,w31,b1);
    for j =1 :4
        neuron{i}{j,2:width(neuron{i})} = Model{j}{2}.userdata.n{i};
        outNeuron{j,2:width(outNeuron)} = Model{j}{2}.userdata.o;
    end
    if(wrtieToExcel)
        cd RunResults
        writetable(neuron{i},'weights.xlsx','sheet','ANN','range',['A',num2str(1+6*(i-1))],'WriteVariableNames',true);
        cd ..
    end
end
if(wrtieToExcel)
    cd RunResults
    writetable(outNeuron,'weights.xlsx','sheet','ANN','range','g1','WriteVariableNames',true);
    cd ..
end

