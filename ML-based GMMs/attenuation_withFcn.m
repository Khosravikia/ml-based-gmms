% Draw attenuation for all models and IMs
%% ANN attenuate
load Models\ANN_4Out_1.mat
attenuate(Model{1}{2},options,1,'ANN_4Out_1_PGA');
attenuate(Model{2}{2},options,2,'ANN_4Out_1_T0_10');
attenuate(Model{3}{2},options,3,'ANN_4Out_1_T0_50');
attenuate(Model{4}{2},options,4,'ANN_4Out_1_T1_00');
clear;
%% RF attenuate
load Models\RF_4Out_1.mat
attenuate(Model{1}{2},options,1,'RF_4Out_1_PGA');
attenuate(Model{2}{2},options,2,'RF_4Out_1_T0_10');
attenuate(Model{3}{2},options,3,'RF_4Out_1_T0_50');
attenuate(Model{4}{2},options,4,'RF_4Out_1_T1_00');
clear;
%% SVR attenuate
load Models\SVR_4Out_1.mat
attenuate(Model{1}{2},options,1,'SVR_4Out_1_PGA');
attenuate(Model{2}{2},options,2,'SVR_4Out_1_T0_10');
attenuate(Model{3}{2},options,3,'SVR_4Out_1_T0_50');
attenuate(Model{4}{2},options,4,'SVR_4Out_1_T1_00');
clear;
%% Lin attenuate
load Models\Lin_4Out_1.mat
attenuate(Model{1}{1},options,1,'Lin_4Out_1_PGA');
attenuate(Model{2}{1},options,2,'Lin_4Out_1_T0_10');
attenuate(Model{3}{1},options,3,'Lin_4Out_1_T0_50');
attenuate(Model{4}{1},options,4,'Lin_4Out_1_T1_00');
clear;
%% paper attenuate
options.useLnR
V = 0;
options.useMapstd = 0;
attenuate("paper",options,1,'paper_PGA');