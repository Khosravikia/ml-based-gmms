
Step 1: Train GMMs with different machine learning techniques and save the models

In this step, users should use MainTrain_AllOutputs.m with each of the possible method options as follows: 
RF: Random Forest; 
ANN: Artificial Neutral Network
SVM: Support Vector Machine
Lin: Linear Regression

Step 2: Once the models are trained and saved, users can run "attenuation_methodCompFull.m" and "attenuation_withFcn.m" to plot the attenuation graphs.

Optional Steps:
ANN_extract_weight.m: extracts the weights of the ANN models
Sensitivity_ANN: trains different ANN models with different modeling assumptions
Sensitivity_RF: trains different RF models with different modeling assumptions
Sensitivity_SVM: trains different SVM models with different modeling assumptions

dataSizeSensitivity: Uses a fraction of data and train the models to investigate the sensitivity of the models to the data size.

Note: The other files in the project folder are functions used in the main MATLAB files.