% Draw attenuation of different models with given range to compare them

clear;
clc;
%%
toSave = 1;
rangeSelection = 5; %zero to plot all ranges

%Data
load 'Raw_data.mat' Raw_data
rData = table;
rData.Mw = Raw_data.Magnitude;
rData.Vs30(:) = 760;
rData.Rhypo = Raw_data.HypocentralDistance_km_;
rData.PGA = 980.665*Raw_data.PGA_g_;

%Models
method = {"ANN","SVR","Lin","RF","paper"};
cd Models;
model = cell(5,1);
option = cell(5,1);
load ANN_4Out_1 Model options
model{1} = Model{1}{2};
option{1} = options;

load SVR_4Out_1 Model options
model{2} = Model{1}{2};
option{2} = options;

load Lin_4Out_1 Model options
model{3} = Model{1}{1};
option{3} = options;

load RF_4Out_1 Model options
model{4} = Model{1}{2};
option{4} = options;

model{5} = "Paper";
options.useLnRV=0;
options.useMapstd=0;
options.useMapminmax = 0;
option{5} = options;
cd ..;


%Colors/Makers:
yellow = [0.87984 0.93798 0.1124];
orange = [0.9098 0.55686 0.035294];
red = [0.92636 0.081395 0.2093];
purple = [0.79457 0.023256 0.81008];
blue = [0.23643 0.046512 0.81008];
colors = {yellow,orange,red,purple,blue};
markers={'s','o','h','d','p'};
%Ranges:
range = cell(5,1);
range{1} = rData(rData.Mw>=3 & rData.Mw<3.5,:);
range{2} = rData(rData.Mw>=3.5 & rData.Mw<4,:);
range{3} = rData(rData.Mw>=4 & rData.Mw<4.5,:);
range{4} = rData(rData.Mw>=4.5 & rData.Mw<5,:);
range{5} = rData(rData.Mw>=5,:);

%% Plot

if(rangeSelection==0)
    i = 1:length(range);
    f1= figure('Units', 'Normalized', 'OuterPosition', [0, 0.05, 0.98, 0.95]);
else
    i = rangeSelection;
    f1= figure('Units', 'Normalized', 'OuterPosition', [0, 0.05, 0.4, 0.70]);
end

for i = i
    if(rangeSelection==0)
        subplot(2,3,i)
    end
    
    axis([3 600 0 300]);
    set(gca,'xscale','log');
    set(gca,'yscale','log');
    xlabel('{\it R}_{hypo} (km)','FontName', 'Times New Roman');
    ylabel('PGA (cm/s)','FontName', 'Times New Roman');
    grid on;
    box on;
    hold on;
    
    scatter(range{i}.Rhypo,range{i}.PGA,30,colors{i});
    
    for k = 1:length(model)
        % lineRange = (4:2:500)';
        if(method{k}=="RF")
            lineRange = [(4:2:14),(16:4:36),(40:8:48),(56:20:236),(256:40:500)]';
            %     lineRange = (4:2:500)';
        elseif(method{k}=="SVR"||method{k}=="paper")
            lineRange = (5:2:500)';
        else
            lineRange = (4:2:500)';
        end
        
        %Plot fixing for RF
        if(method{k}=="RF")
            stroedLineRange = lineRange;
            changedLineRange = 1;
            %lineRange = [(4:2:14),(16:4:36),(40:8:48),(56:20:236),(256:40:500)]';
            switch i
                case 1
                    lineRange = [(4:2:14),(16:4:36),48,(56:20:176),(216:20:236),(256:40:500)]';
                case 3
                    lineRange = [(4:2:14),(16:4:36),(40:8:48),(56:20:94),(176:20:236),(256:40:500)]';
                case 4
                    lineRange = [(4:2:14),(16:4:36),(40:8:48),(56:20:136),(176:20:236),(256:40:500)]';
                case 5
                    lineRange = [(4:2:14),(16:4:36),(40:8:48),(56:20:236),(256:40:376),(456:40:500)]';
            end
        end
        
        %Get data ready
        Mw = ones(length(lineRange),1)*(2.75 + 0.5*i);
        %Nomalize
        if(option{k}.useLnRV)
            Rhypo = log(lineRange);
            Vs30 = ones(length(lineRange),1)*log(760);
        else
            Rhypo = lineRange;
            Vs30 = ones(length(lineRange),1)*(760);
        end
        
        predicti = table(Mw,Vs30,Rhypo);
        
        %Predict
        if(method{k}=="ANN")
            predicti.PredictedOutput = model{k}(predicti{:,1:3}')';
        elseif(method{k}=="Lin")
            predicti.Index(:) = 100;
            predicti.Station(:) = {'T35B'};
            predicti.PredictedOutput = predict(model{k},predicti,'Conditional',false);
        elseif(method{k}=="paper")
            predicti.PredictedOutput = paperFnc(predicti,1,1);
        else
            predicti.PredictedOutput = predict(model{k},predicti);
        end
        
        if(option{k}.useLnRV)
            predicti.Rhypo = exp(predicti.Rhypo);
        end
    
        %Plot
        if(method{k}=="RF")
            switch i
                case 1
                    MarkerIndices = [8 13 15  21 29];
                case 2
                    MarkerIndices = [8 14 16  22 31];
                case 3
                    MarkerIndices = [8 14 16  19 27];
                case 4
                    MarkerIndices = [8 14 16  22 30];
                case 5
                    MarkerIndices = [8 14 16  22 30];
            end
        else
            MarkerIndices = floor(logspace(1,log10(length(predicti.Rhypo)),5));
        end
        
        p{k} = plot(predicti.Rhypo,exp(predicti.PredictedOutput),'-.','color','k','LineWidth',3,...
            'MarkerIndices',MarkerIndices,...
            'MarkerSize',10,'Marker',markers{k},'MarkerEdgeColor','k', 'MarkerFaceColor',colors{k});
        
        if(method{k}=="RF" && changedLineRange)
            lineRange = stroedLineRange;
            changedLineRange = 0;
        end
        
    end
    legend ([p{1},p{2},p{3},p{4},p{5}],"ANN","SVR","Lin","RF","paper",'Location','southwest');

end

if(toSave)
   cd RunResults/Attenuation/Method_Compare
   saveas(f1,['FullCompare_',num2str(rangeSelection)]);
    saveas(f1,strcat("FullCompare_",num2str(rangeSelection),".jpg"));
    cd ..
    cd ..
    cd ..
end