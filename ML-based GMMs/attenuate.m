function [] = attenuate(model,options,output,name)
%
% Draw (and) save attenuation for given model and output

% Detecting method
if(isa(model,'network'))
   method = "ANN";
elseif(isa(model,'TreeBagger'))
    method = "RF";
elseif(isa(model,'RegressionSVM'))
    method="SVR";
elseif(isa(model,'LinearMixedModel'))
    method="Lin";
elseif(isa(model,'classreg.learning.regr.RegressionBaggedEnsemble')|| isa(model,'classreg.learning.regr.CompactRegressionEnsemble'))
    method="RF";
elseif(model=="paper")
    method="paper";
end


% Detecting empty name
if(strcmp(name,''))
    toSave = 0;
else
    toSave = 1;
end

%Choosing steps
if(method=="SVR"||method=="paper")
    lineRange = (5:2:500)';
else
    lineRange = (4:2:500)';
end
% lineRange = logspace(log10(4),log10(500),(500-4)/9)';

%Data
load 'Raw_data.mat' Raw_data
rData = table;
rData.Mw = Raw_data.Magnitude;
rData.Vs30(:) = 760;
rData.Rhypo = Raw_data.HypocentralDistance_km_;

switch output
    case 1
        rData.Output = 980.665*Raw_data.PGA_g_;
        label = 'PGA (cm/s)';
    case 2
        rData.Output = Raw_data.T0_10s;
        label = 'PSA (0.10s)';
    case 3
        rData.Output = Raw_data.T0_50s;
        label = 'PSA (0.50s)';
    case 4
        rData.Output = Raw_data.T1_00s;
        label = 'PSA (1.00s)';
end

rData.Index = Raw_data.Index;
rData.Station = Raw_data.Station;


%Colors/Makers:
yellow = [0.87984 0.93798 0.1124];
orange = [0.9098 0.55686 0.035294];
red = [0.92636 0.081395 0.2093];
purple = [0.79457 0.023256 0.81008];
blue = [0.23643 0.046512 0.81008];
colors = {yellow,orange,red,purple,blue};
markers={'s','o','h','d','p'};
%Ranges:
range = cell(5,1);
range{1} = rData(rData.Mw>=3 & rData.Mw<3.5,:);
range{2} = rData(rData.Mw>=3.5 & rData.Mw<4,:);
range{3} = rData(rData.Mw>=4 & rData.Mw<4.5,:);
range{4} = rData(rData.Mw>=4.5 & rData.Mw<5,:);
range{5} = rData(rData.Mw>=5,:);
%Process Settings
if(options.useMapstd)
    [~, MPS] = mapstd(rData.Mw');
    [~, VPS] = mapstd(rData.Vs30');
    [~, RPS] = mapstd(rData.Rhypo');
    [~,PS] = mapstd(log(rData.Output)');
end
%% Plot
f1= figure('Units', 'Normalized', 'OuterPosition', [0, 0.05, 0.4, 0.70]);
axis([3 600 0 300]);
set(gca,'xscale','log');
set(gca,'yscale','log');
xlabel('{\it R}_{hypo} (km)','FontName', 'Times New Roman');
ylabel(label,'FontName', 'Times New Roman');
grid on;
box on;
hold on;

numOfrange = length(range);
%Plost Scatters
for i = 1:numOfrange
    scatter(range{i}.Rhypo,range{i}.Output,30,colors{i}); %'filled'
end

%Predict and plot Lines
p = cell(numOfrange,1);

for i = 1:numOfrange
% for i = 5:5
    %Get data ready
    Mw = ones(length(lineRange),1)*(2.75 + 0.5*i);
    %Nomalize
    if(options.useLnRV)
        Rhypo = log(lineRange);
        Vs30 = ones(length(lineRange),1)*log(760);
    else
        Rhypo = lineRange;
        Vs30 = ones(length(lineRange),1)*(760);
    end
    
    predicti = table(Mw,Vs30,Rhypo);
    
    if(options.useMapstd)
        [mapped, ~] = mapstd('apply',predicti.Rhypo',RPS);
        predicti.Rhypo = mapped';
        predicti.Mw(:) =mapstd('apply',predicti.Mw(1),MPS);
        predicti.Vs30(:) =mapstd('apply',predicti.Vs30(1),VPS);
    end
    
    %Predict
    if(method=="ANN")
        predicti.PredictedOutput = model(predicti{:,1:3}')';
    elseif(method=="Lin")
        predicti.Index(:) = 200;
        predicti.Station(:) = {'T35B'};
        predicti.PredictedOutput = predict(model,predicti,'Conditional',false);
    elseif(method=="paper")
        predicti.PredictedOutput = paperFnc(predicti,1,1);
    else
        predicti.PredictedOutput = predict(model,predicti);
    end
    
    %De-Nomalize
    if(options.useMapstd)
        unmapped = mapstd('reverse',predicti.PredictedOutput',PS);
        predicti.PredictedOutput = unmapped';
        unmapped = mapstd('reverse',predicti.Rhypo',RPS);
        predicti.Rhypo = unmapped';
    end
    if(options.useLnRV)
        predicti.Rhypo = exp(predicti.Rhypo);
    end
    
    %Plot
     p{i} = plot(predicti.Rhypo,exp(predicti.PredictedOutput),'-.','color','k','LineWidth',3,...
         'MarkerIndices',floor(logspace(1,log10(length(predicti.Rhypo)),5)),...
         'MarkerSize',10,'Marker',markers{i},'MarkerEdgeColor','k', 'MarkerFaceColor',colors{i});
end
legend ([p{1},p{2},p{3},p{4},p{5}],'M: 3.0-3.5','M: 3.5-4.0','M: 4.0-4.5','M: 4.5-5.0','M: 5.0-5.8','Location','southwest');

if(toSave)
    cd RunResults/Attenuation/
    saveas(f1,name);
    saveas(f1,strcat(name,".jpg"));
    cd ..
    cd ..
    close;
end

