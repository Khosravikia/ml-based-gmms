% Sensitivity Analysis

clear;
clc;
id = 'MATLAB:xlswrite:AddSheet';
warning('off', id)
disp(['Started: ',datestr(datetime,'HH:MM:SS')]);
%% Database read and options set

%%%%%%%%%%%%%% Do NOT change this part%%%%%%%%%%%%%
%File options:
toPlot = 0; %Warning: Plot includes save inside its function!
toSaveExcel = 1;
testModel = 0;
toSaveModel = 0;


%Default values
options.useLnRV = 0;

%%%%%%%%%%%%%%% Options %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%(Normalizations: options.useMapminmax|options.useMapstd|options.useLnRV)%%
%Connect to excel database?
DataBase = 0;
%Include station as random effect?
options.includeStation = 1;

%Maximum number of iterations 
%!!!!Warning!!!: Excel write ranges are according to 10 iterations
maxIteration = 2;

%Model variable setting:
numTrees = [5 40 80 120 160 200];
MinLeafSize = [2 3 4 6];
InBagFraction = [0.66, 0.75, 1];


%Static Settings
options.NumPredictorstoSample = 2;  %Default: 2
options.Prune = 'off';               %Default: off
options.useMapstd = 0;
options.KFoldMethod = "Manual"; %"System" | "Manual"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Read and/or load database
if(DataBase)
    cd ..
    Raw_data = readtable('GroundMotionDatabase-1.xlsx');
    cd Summary1
    Raw_data(Raw_data.HypocentralDistance_km_<4,:) = [];
    Raw_data(Raw_data.HypocentralDistance_km_>500,:)=[];
    Raw_data(Raw_data.Magnitude<2.8,:)=[];
    save 'Raw_data.mat' Raw_data
end
load 'Raw_data.mat'

%Getting data-set ready
dataN2 = table;
dataN2.Mw = Raw_data.Magnitude;
if(options.useLnRV)
    dataN2.Vs30 = log(Raw_data.VS30_m_s_);
    dataN2.Rhypo = log(Raw_data.HypocentralDistance_km_);
else
    dataN2.Vs30 = Raw_data.VS30_m_s_;
    dataN2.Rhypo = Raw_data.HypocentralDistance_km_;    
end
dataN2.PGA = log(980.665*Raw_data.PGA_g_);
dataN2.Index = Raw_data.Index;
dataN2.Station = Raw_data.Station;

%% Preparing data and Iterations
% for x = 1:length(InBagFraction)
for x = 3:3
    options.InBagFraction =InBagFraction(x);
    for j = 1 : length(numTrees)
%     for j = 3:3
        %Number of bagged Trees
        options.numTrees = numTrees(j);
        
        for z = 1 : length(MinLeafSize)
%         for z = 2 : 2
            %Minimum number of leaf observations
            options.MinLeafSize = MinLeafSize(z);
            
            
            %Get dataSet Ready for iterations
            dataSet = dataN2;
            dataSet.newPGA = dataSet.PGA;
            
            %Tables/Cells to save results/models:
            if(options.includeStation)
                [Lh, stdIndex , stdStation , stdEps,RMS ,Ri, LhchangeRate, iterat, sqrtstd, KFoldRMS] = deal(zeros(maxIteration+1,1));
                LhResults = table(Ri, RMS, stdIndex , stdStation , stdEps,iterat, Lh,sqrtstd, KFoldRMS, LhchangeRate);
            else
                [Lh, stdIndex , stdStation , stdEps,RMS ,Ri, LhchangeRate, iterat, sqrtstd, KFoldRMS] = deal(zeros(maxIteration+1,1));
                LhResults = table(Ri, RMS, stdIndex , stdEps,iterat, Lh,sqrtstd, KFoldRMS,  LhchangeRate);
            end
            [Model, lme] = deal(cell(maxIteration,1));
            
            %Create table to store times
            [fixedT, RET, part1T, part2T, part3T,sumParts, totalT, iterat] = deal(zeros(maxIteration+1,1));
            TResults = table(iterat, fixedT, RET, part1T, part2T, part3T,sumParts, totalT);
            
            %Start point for while
            k = 1;
            LHchangeRate{k} = 100;
            bestLHIteration = 1;
            stopSearchLh = 0;
            %Making cells of variable to later use them for best iteration
            %results
            [stdEtaIndex,eta,stdEtaStation,residual,stdE,RMSE,R,sqr,KFoldRMSE] = deal(cell(maxIteration,1));
            Likelihood = zeros(maxIteration,1);
            
            while(true && k<=maxIteration)
                TResults.iterat(k) = k;
                tic;
                
                TResults.part1T(k) = toc;
                tic;
                %Train and prediction
                if(options.KFoldMethod=="System")
                    [~ ,KFoldRMSE{k} ,Model{k}] = trainRF(dataSet,options.numTrees,options.MinLeafSize,...
                        options.NumPredictorstoSample,options.InBagFraction);
                elseif(options.KFoldMethod == "Manual")
                    [~ ,~ ,Model{k}] = trainRF(dataSet,options.numTrees,options.MinLeafSize,...
                        options.NumPredictorstoSample,options.InBagFraction);
                    dataSet.KfoldIndices = crossvalind('Kfold',height(dataSet),5);
                    KFRMSE= zeros(5,1);
                    for i  = 1:5
                        [~ ,~ ,TempModel] = trainRF(dataSet(dataSet.KfoldIndices ~=i,:),options.numTrees,options.MinLeafSize,...
                            options.NumPredictorstoSample,options.InBagFraction);
                        TempPredicted = predict(TempModel,dataSet(dataSet.KfoldIndices ==i,:));
                        TempResidual = dataSet(dataSet.KfoldIndices ==i,:).PGA - TempPredicted;
                        KFRMSE(i) = std(TempResidual);
                    end
                    KFoldRMSE{k}= mean(KFRMSE);
                end
                dataSet.PredictedPGA = predict(Model{k},dataSet);
                
                
                
                TResults.fixedT(k) = toc;
                
                tic;
                
                %Storing residual (for random effect regression)
                dataSet.residual = dataSet.PGA - dataSet.PredictedPGA;
                TResults.part2T(k) = toc;
                
                tic;
                %Train random effect model
                
                if(options.includeStation)
                    lme{k} = fitlme(dataSet, 'residual ~ 1+(1|Index)+(1|Station)');
                else
                    lme{k} = fitlme(dataSet, 'residual ~ 1+(1|Index)');
                end
                
                TResults.RET(k) = toc;
                
                tic;
                %Calculate random effect parameters
                Likelihood(k) = lme{k}.LogLikelihood;
                [eta{k},etaWtihNames] = randomEffects(lme{k});
                etaWtihNames.eta = eta{k};
                stdEtaIndex{k} = std(etaWtihNames(strcmp(etaWtihNames.Group,'Index'),:).eta);
                if(options.includeStation)
                    stdEtaStation{k} = std(etaWtihNames(strcmp(etaWtihNames.Group,'Station'),:).eta);
                end
                
                %Likelihood change
                if(k>1)
                    LHchangeRate{k} = (Likelihood(k)-Likelihood(k-1))*100/abs(Likelihood(k-1));
                    
                    %Select the best iteration when change rate is below 5%
                    if(abs(LHchangeRate{k})<5 && ~stopSearchLh)
                        bestLHIteration = find(Likelihood == max(Likelihood(1:k-1)));
                        stopSearchLh = 1;
                    end
                end
                
                %Draw residuals without random effect
                if(k==1)
                    dataSet.residual = dataSet.PGA - dataSet.PredictedPGA;
                    Raw_data.residual = dataSet.residual;
                    if(toPlot)
                        ploti(Raw_data,dataSet,k,strcat(method,"_","S",num2str(options.includeStation)),0);
                    end
                end
                
                %Prepare PGA for next iteration
                for i = 1:length(dataSet.newPGA)
                    etaIndex = etaWtihNames(strcmp(etaWtihNames.Level , num2str(dataSet(i,:).Index)),:).eta;
                    if(options.includeStation)
                        etaStation = etaWtihNames(strcmp(etaWtihNames.Level , dataSet(i,:).Station),:).eta;
                        dataSet(i,:).newPGA = dataSet(i,:).PGA - etaIndex - etaStation;
                    else
                        dataSet(i,:).newPGA = dataSet(i,:).PGA - etaIndex;
                    end
                end
                
                
                %Calculating residual (y - f - eta) and RMSE/R/Sqr(etas)
                residual{k} = dataSet.newPGA-dataSet.PredictedPGA;
                stdE{k} = std(residual{k});
                RMSE{k} = std(dataSet.PGA - dataSet.PredictedPGA);
                [~, Ri, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~] = Regi(dataSet.PGA,dataSet.PredictedPGA,'Temp');
                R{k} = Ri;
                if(options.includeStation)
                    sqr{k} = sqrt(stdEtaIndex{k}^2 + stdEtaStation{k}^2 + stdE{k}^2);
                else
                    sqr{k} = sqrt(stdEtaIndex{k}^2 + stdE{k}^2);
                end
                
                
                %Display and save result
                if(options.includeStation)
%                     disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) , ' stdEtaStation: ' , num2str(stdEtaStation{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}) , ' R: ' ,  num2str(R{k}), ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k), ' sqr: ', num2str(sqr{k})]);
                    LhResults{k,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} , stdEtaStation{k} ,stdE{k},k , Likelihood(k),sqr{k},KFoldRMSE{k}, LHchangeRate{k}];
                else
%                     disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) , ' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}) , ' R: ' ,  num2str(R{k}), ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k), ' sqr: ', num2str(sqr{k})]);
                    LhResults{k,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} ,stdE{k} ,k, Likelihood(k),sqr{k},KFoldRMSE{k}, LHchangeRate{k}];
                end
                
                TResults.part3T(k) = toc;
                TResults.sumParts(k) = TResults.part1T(k) + TResults.part2T(k)+ TResults.part3T(k);
                TResults.totalT(k)= TResults.sumParts(k) + TResults.fixedT(k) + TResults.RET(k);
                
                k = k+1;
            end
            
            %% After iteration
            
            %Check for 20% better likelihood
            bestLh = Likelihood(bestLHIteration);
            for i = (bestLHIteration+1):length(Likelihood)
                changeRate = 100*(Likelihood(i)-bestLh)/abs(bestLh);
                if(changeRate>20)
%                     disp(['Best Iteration changed from', num2str(bestLHIteration), ' to ' , num2str(i)]);
                    bestLHIteration = i;
                    bestLh = Likelihood(i);
                end
            end
            k = bestLHIteration;
            
            %Storing residual of iteration with max LH (storing residual at Raw_data in needed at plots)
            dataSet.residual = residual{k};
            Raw_data.residual = dataSet.residual;
            
            %Display and save best iteration
%             disp('Best likelihood on: ');
            if(options.includeStation)
%                 disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}), ' stdEtaStation: ' , num2str(stdEtaStation{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}),' R: ' ,  num2str(R{k}) , ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k)]);
                LhResults{maxIteration+1,:} = [R{k},RMSE{k} ,  stdEtaIndex{k} , stdEtaStation{k} ,stdE{k},k , Likelihood(k),sqr{k},KFoldRMSE{k}, LHchangeRate{k}];
            else
%                 disp([' LogLikelihood: ' , num2str(Likelihood(k)), ' stdEtaIndex: ' , num2str(stdEtaIndex{k}) ,' stdE: ' ,num2str(stdE{k}), ' RMSE: ' ,  num2str(RMSE{k}),' R: ' ,  num2str(R{k}) , ' %LHchangeRate: ' , num2str(LHchangeRate{k}) , ' Iteration: ' , num2str(k)]);
                LhResults{maxIteration+1,:} = [R{k},RMSE{k} ,  stdEtaIndex{k}  ,stdE{k},k, Likelihood(k) ,sqr{k},KFoldRMSE{k}, LHchangeRate{k}];
            end
            if(toPlot)
                ploti(Raw_data,dataSet,k,strcat(method,"_b_",num2str(k),"S",num2str(options.includeStation)),0);
            end
            
            %Save time results
            lastRow = maxIteration + 1;
            TResults.fixedT(lastRow) = mean(TResults.fixedT(1:maxIteration));
            TResults.RET(lastRow) = mean(TResults.RET(1:maxIteration));
            TResults.sumParts(lastRow) = mean(TResults.sumParts(1:maxIteration));
            TResults.totalT(lastRow) = mean(TResults.totalT(1:maxIteration));
            
            %Export results to excel
            sum = [LhResults(1,:);LhResults(height(LhResults),:)];
            
            %Write Range according to transfer functions
            switch z
                case 1
                    writeRange = 'A1';
                case 2
                    writeRange = 'L1';
                case 3
                    writeRange = 'W1';
                case 4
                    writeRange = 'A14';
                case 5
                    writeRange = 'L14';
                case 6
                    writeRange = 'W14';
                case 7
                    writeRange = 'A27';
                case 8
                    writeRange = 'L27';
                case 9
                    writeRange = 'W27';
            end          
            
            
            if(testModel)
                name = strcat(name,"Test");
            end
            
            if(options.KFoldMethod=="System")
                altName='';
            elseif(options.KFoldMethod=="Manual")
                altName='_ManKf2_';
            end
            
            if(toSaveExcel)
%                 disp('Saving Excels...');
                cd RunResults/Sensitivity
                %writetable(sum,'Sensitivity_ANN_Sum.xlsx','sheet',sheet,'range',writeRange,'WriteVariableNames',true);
                writetable(LhResults,['Sensitivity_RF_',altName,num2str(InBagFraction(x)),'.xlsx'],'sheet',strcat("NT_",num2str(j)),'range',writeRange);
                %writetable(TResults,'Times.xlsx','sheet',name,'range','A1');
                cd ..
                cd ..
            end
            
            if(toSaveModel)
                disp('Saving Model...');
                cd Models;
                save(name,'eta','LhResults','lme','Model','options','residual','TResults');
                cd ..;
            end
%             disp('Done!');
            disp(['    Finished Min Leaf Size: ',num2str(z),' | ',datestr(datetime,'HH:MM:SS')]);
        end
        disp(['  Finished Num Tree: ',num2str(j),' | ',datestr(datetime,'HH:MM:SS')]);
    end
    disp(['Finished In Bag Fraction: ',num2str(x),' | ',datestr(datetime,'HH:MM:SS')]);
end