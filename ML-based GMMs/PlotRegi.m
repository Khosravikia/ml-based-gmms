% Use Rego_plot.m on all models
clear;
clc;
%%

toSaveEMF = 1;
%RValues to show in figures. Set to 0 to be calculated. Check order with order of mode{1-5}
RValues = [0.915,0.921,0.895,0.958,0.909];
cd 'C:\Users\UR\Desktop\Models'

%Models
method = {"ANN","SVR","Lin","RF","paper"};
cd 'C:\Users\UR\Desktop\Models';
model = cell(5,1);
option = cell(5,1);
load ANN_4Out_1_Bak Model options
model{1} = Model{1}{2};
option{1} = options;

load SVR_4Out_1 Model options
model{2} = Model{1}{2};
option{2} = options;

load Lin_4Out_1 Model options
model{3} = Model{1}{1};
option{3} = options;

load RF_4Out_1 Model options
model{4} = Model{1}{2};
option{4} = options;

model{5} = "Paper";
options.useLnRV=0;
options.useMapstd=0;
options.useMapminmax = 0;
option{5} = options;
cd 'C:\Users\UR\OneDrive\ML\Matlab\Summary1';

load 'Raw_data.mat'
%%

for i = 1:length(model)
% for i = 1:1
    options = option{i};
    
    dataN2 = table;
    dataN2.Mw = Raw_data.Magnitude;
    if(options.useLnRV)
        dataN2.Vs30 = log(Raw_data.VS30_m_s_);
        dataN2.Rhypo = log(Raw_data.HypocentralDistance_km_);
    else
        dataN2.Vs30 = Raw_data.VS30_m_s_;
        dataN2.Rhypo = Raw_data.HypocentralDistance_km_;
    end
    dataN2.PGA = log(980.665*Raw_data.PGA_g_);
    
    dataSet = dataN2;
    dataSet.newPGA = dataSet.PGA;
    %Predict
    if(method{i} == "RF")
        dataSet.PredictedPGA = predict(model{i},dataSet);
    elseif(method{i} == "ANN")
        dataSet.PredictedPGA = model{i}(dataSet{:,1:3}')';
    elseif(method{i} == "SVR")
        dataSet.PredictedPGA = predict(model{i},dataSet);
    elseif(method{i}=="Lin")
        dataSet.Index(:) = 100;
        dataSet.Station(:) = {'T35B'};
        dataSet.PredictedPGA = predict(model{i},dataSet,'Conditional',false);
    elseif(method{i}=="paper")
        dataSet.PredictedPGA = paperFnc(dataSet,1,1);
    end
    
    dataSet.residual = dataSet.PGA - dataSet.PredictedPGA;
    Raw_data.residual = dataSet.residual;
    
    name = strcat(method{i},"_Regi");
    [R{i},fig{i}] = Regi_Plot(dataSet.PGA,dataSet.PredictedPGA,RValues(i));
    if(toSaveEMF)
        cd 'C:\Users\UR\Desktop\EMFPics\Regi'
        saveas(fig{i},strcat(name,"_g.emf"));
        cd 'C:\Users\UR\OneDrive\ML\Matlab\Summary1';
    end
    close(fig{i});
end